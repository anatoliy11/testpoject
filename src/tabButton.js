import React from 'react';

const TabButton = (props) => (
  <div className={'tabButton ' + (props.isSelecting ? 'selecting' : '')}
       onClick={props.clickHandler}>TAB {props.index}</div>
);

TabButton.propTypes = {
  index: React.PropTypes.number,
  clickHandler: React.PropTypes.func,
  isSelecting: React.PropTypes.bool
};

export default TabButton;