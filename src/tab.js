import React from 'react';
import Table from './table';
export default class Tab extends React.Component {
  render() {
    return (
      <Table searchString={this.props.searchString} data={this.props.tableData} fieldsDescription={this.props.fieldsDescription}/>
    )
  }
}

Tab.propTypes = {
  tableData: React.PropTypes.array,
  fieldsDescription: React.PropTypes.array,
  searchString: React.PropTypes.string
};