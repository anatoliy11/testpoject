import React from 'react';
import Tab from './tab';
import Axios from 'axios';
import TabButton from './tabButton';
import SearchPanel from './searchPanel';

export default class TabPanel extends React.Component {

  state = {
    tenders: [],
    companies: [],
    tabIndex: 0,
    searchString: ''
  };

  fieldsDescripton1 = [
    {
      name: 'Name',
      sysName: 'name',
      width: '50%',
      type: 'string'
    },
    {
      name: 'Inn',
      sysName: 'inn',
      width: '20%',
      type: 'string'
    },
    {
      name: 'Kpp',
      sysName: 'kpp',
      width: '30%',
      type: 'string'
    }
  ];

  fieldsDescripton2 = [
    {
      name: 'Name',
      sysName: 'name',
      width: '25%',
      type: 'string'
    },
    {
      name: 'Price',
      sysName: 'price',
      width: '15%',
      type: 'number'
    },
    {
      name: 'Phase',
      sysName: 'phase',
      width: '15%',
      type: 'string'
    },
    {
      name: 'PublicationDate',
      sysName: 'publicationdate',
      width: '15%',
      type: 'date'
    },
    {
      name: 'RegionName',
      sysName: 'regionName',
      width: '15%',
      type: 'string'
    },
    {
      name: 'Subcategory',
      sysName: 'subcategory',
      width: '15%',
      type: 'string'
    },
  ];

  componentDidMount() {
    ['Table1.json', 'Table2.json'].forEach(async (url) => {
      let {data} = await Axios.get(url);
      this.setState(data);
    })
  }

  render() {
    const tabs = [
      <Tab searchString={this.state.searchString}
           tableData={this.state.companies}
           fieldsDescription={this.fieldsDescripton1}/>,
      <Tab searchString={this.state.searchString}
           tableData={this.state.tenders}
           fieldsDescription={this.fieldsDescripton2}/>
    ];
    return (
      <div>
        {tabs.map((item, tabIndex) =>
          <TabButton isSelecting={tabIndex === this.state.tabIndex}
                     clickHandler={() => this.setState({tabIndex})} index={tabIndex}/>
        )}
        <SearchPanel changeHandler={(e) => this.setState({searchString: e.target.value})}
                     searchString={this.state.searchString}/>
        {tabs[this.state.tabIndex]}
      </div>
    )
  }
}