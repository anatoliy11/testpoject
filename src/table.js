import React from 'react';
import Row from './row';
import HRow from './hrow';

export default class Table extends React.Component {

  state = {
    sorting: '',
    direction: 0,
    sortFieldType: 'string'
  };

  sort(fieldName, type) {
    this.setState({sortFieldType: type});
    if (fieldName === this.state.sorting && this.state.direction === 1) {
      this.setState({sorting: fieldName, direction: 2});
    } else if (fieldName === this.state.sorting && this.state.direction === 2) {
      this.setState({sorting: '', direction: 0});
    } else {
      this.setState({sorting: fieldName, direction: 1});
    }
  }

  comparator(a, b) {
    let a1 = a[this.state.sorting] || '';
    let b1 = b[this.state.sorting] || '';

    switch (this.state.sortFieldType) {
      case 'string':
        a1 = (a1 + '').toUpperCase();
        b1 = (b1 + '').toUpperCase();
        break;
      case 'number':
        a1 = +(a1.replace(/ /g, ''));
        b1 = +(b1.replace(/ /g, ''));
        break;
      case 'date':
        a1 = Date.parse(a1.replace(/(\d+)\.(\d+)\.(\d+)/,"$2/$1/$3"));
        b1 = Date.parse(b1.replace(/(\d+)\.(\d+)\.(\d+)/,"$2/$1/$3"));
        break;
    }

    if (!!this.state.sorting && this.state.direction === 1) {
      return (a1 > b1) ? -1 : 1;
    }
    if (!!this.state.sorting && this.state.direction === 2) {
      return (a1 < b1) ? -1 : 1;
    }
    return 0;
  }

  filterFunc(item) {
    return !!this.props.fieldsDescription.filter((field) => ~item[field.sysName].toUpperCase().indexOf(this.props.searchString.toUpperCase())).length;
  }

  render() {
    return (
      <table>
        <thead>
          <HRow sortState={this.state.direction}
                sortField={this.state.sorting}
                fieldsDescription={this.props.fieldsDescription}
                clickHandler={(field, type) => this.sort(field, type)}/>
        </thead>
        <tbody>
          {this.props.data.map(a => a)
            .filter((item) => this.filterFunc(item))
            .sort((a, b) => this.comparator(a, b))
            .map((row) => <Row fieldsDescription={this.props.fieldsDescription}
                               data={row}
                               key={row.Id}/>)}
        </tbody>
      </table>
    )
  }
}

Table.propTypes = {
  fieldsDescription: React.PropTypes.array,
  data: React.PropTypes.array,
  searchString: React.PropTypes.string
};