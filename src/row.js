import React from 'react';

const Row = (props) => (
  <tr>
    {props.fieldsDescription.map(({sysName, width}) => <td key={sysName}
                                                           width={width}>{props.data[sysName]}</td>)}
  </tr>
);

Row.propTypes = {
  fieldsDescription: React.PropTypes.array,
  data: React.PropTypes.any
};

export default Row;