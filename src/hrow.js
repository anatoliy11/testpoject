import React from 'react';

const HRow = (props) => (
  <tr>
    {props.fieldsDescription.map(({name, width, sysName, type}) =>
      <th key={name} width={width} onClick={() => props.clickHandler(sysName, type)}>
        {name}<i className={'fa ' + 'pull-right ' + (props.sortState === 0 || props.sortField !== sysName ? 'fa-sort' : props.sortState === 1 ? 'fa-sort-desc' : 'fa-sort-asc')}/>
      </th>)}
  </tr>
);
HRow.propTypes = {
  fieldsDescription: React.PropTypes.array,
  clickHandler: React.PropTypes.func,
  sortState: React.PropTypes.number,
  sortField: React.PropTypes.string
};

export default HRow;