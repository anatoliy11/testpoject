import React from 'react';

const SearchPanel = (props) => (
  <div className="searchPanel">
    <label>Search</label>
    <input value={props.searchString}
           onChange={props.changeHandler}/>
  </div>
);

SearchPanel.propTypes = {
  changeHandler: React.PropTypes.func,
  searchString: React.PropTypes.string
};

export default SearchPanel;