const webpack = require('webpack');
const Path = require('path');
module.exports = {
  entry: ['babel-polyfill', './src/index.js'],
  output: {
    path: Path.resolve(__dirname, "./build"),
    filename: "bundle.js",
    publicPath: "/build/"
  },
  resolve: {
    extensions: ['.js', '.jsx']
  },
  module: {
    loaders: [
      {
        test: /\.js?$/,
        loader: 'babel-loader',
        exclude: ['node_modules']
      }
    ]
  },
  devtool: "source-map",
  watch: true
};